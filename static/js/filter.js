$(document).ready(function(){
		
	// Select all <td> elements that have a class. The idea
	// here is to replace PASS, Skipped and all other classes
	// for their CSS equivalent
	$('table tbody').find('td').each(function(){
		var $this = $(this);
		var el_class = $this.attr("class");

		if (!el_class)
			return true;
		
		if (el_class.toLowerCase().startsWith('pass'))
			$this.attr('class', 'pwc-success');
		else if (el_class.toLowerCase().startsWith('skip'))
			$this.attr('class', 'pwc-amarelo');
		else if (el_class.toLowerCase().startsWith('fail'))
			$this.attr('class', 'pwc-danger');
	});
		
	// Show screen shot as a modal
	$('a.screen-shot').on("click", function(evt) {
		evt.preventDefault();
		$('#screen-shot-modal-img').attr('src', $(this).attr('href'));
		$('#screen-shot-modal').modal('show'); 
	});
	// If user clicks on the image it'll open in another tab
	$('#screen-shot-modal-img').on('click', function() {
		window.open($(this).attr('src'));
	});
	
	// Initialize DataTable
	var table = $('table.datatable').DataTable({
	"bSort": false,
	"columns": [
		{ "width": "5%", "class": 'short-text-column' },
		{ "width": "25%", "class": 'long-text-column' },
		{ "width": "50%", "class": 'long-text-column' },
		{ "width": "5%", "class": 'short-text-column' },
		{ "width": "5%", "class": 'short-text-column' },
		{ "width": "5%", "class": 'short-text-column' },
		{ "width": "5%", "class": 'short-text-column' },
	],
	"fixedColumns": true
	});

});